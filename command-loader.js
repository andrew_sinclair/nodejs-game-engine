(function() {
  var commands = {
    'h': move_left,
    'j': move_down,
    'k': move_up,
    'l': move_right
  }

  function move_left(gamestate) {
    gamestate.player.move_left();
  }

  function move_down(gamestate) {
    gamestate.player.move_down();
  }

  function move_up(gamestate) {
    gamestate.player.move_up();
  }

  function move_right(gamestate) {
    gamestate.player.move_right();
  }

  modules.export = function() {
    return commands;
  };
})();