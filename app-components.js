(function() {
  var YAML = require('yamljs')
    , fileConfig = YAML.load(process.argv[2] || 'default-app-components.yml');

  function getFilePathName(fileName) {
    return './' + fileConfig[fileName];
  }

  function requireFile(pathName) {
    return require(getFilePathName(pathName));
  }

  module.exports = {
    commandLoader: requireFile('commandLoader'),
    sceneryProvider: requireFile('sceneryProvider'),
    configLoader: requireFile('configLoader'),
    inputHandler: requireFile('inputHandler'),
    inputProvider: requireFile('inputProvider')
  };

})();