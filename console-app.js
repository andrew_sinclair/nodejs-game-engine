(function() {
  var mainApp = require('./game-engine.js')
    , appComposer = require('./app-components.js');

  mainApp.init({
    commandLoader: appComposer.commandLoader,
    cellProducer: appComposer.sceneryProvider,
    configLoader: appComposer.configLoader,
    onInputHandler: appComposer.inputHandler(mainApp),
    inputProvider: appComposer.inputProvider
  });

  mainApp.beginProcess();
})();
