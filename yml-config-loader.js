(function() {
  var colorsLibraryName = 'colors';
  var color = require(colorsLibraryName);
  var util = require('./game-utils');
  var nativeObject = util.loadYaml('app-config.yml');

  function applyStringColor(strings, color) {
    color = color || require(colorsLibraryName);
    for(var string in strings) {
      string[color];
    }
  }

  applyStringColor(nativeObject.borders, nativeObject.border_color)

  module.exports = function() {
    return nativeObject;
  };

})();