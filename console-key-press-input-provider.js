(function() {
  var onInput;
  var keypress = require('keypress');
  var stdin = process.stdin;

  stdin.setRawMode(true);
  stdin.resume();
  keypress(stdin);

  module.exports = {
    registerEventListener: function(onInputHandler) {
      stdin.on('keypress', onInputHandler);
    },
    onExitHandler: function() {
      return process.stdin.pause();
    }
  };
  
})();