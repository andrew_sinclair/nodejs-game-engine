(function() {
  var utils = require('./game-utils.js')
    , color = require('colors');

  var randomScenery = [' ', ' ', ' ', ' ', ' ', '#'.inverse]
    , screenContent = null
    , sceneryProvider = utils.getRandomStream(randomScenery).get;

  module.exports = sceneryProvider;
})();