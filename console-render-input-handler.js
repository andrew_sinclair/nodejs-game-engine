(function() {

  function getCommand(commands, key) {
    function valOrEmpty(test, val) {
      return (test && val) || '';
    }

    var keyName = valOrEmpty(key, valOrEmpty(key.ctrl, '^') + key.name);

    return commands[keyName];
  }

  module.exports = function(app) {
    return function(ch, key) {
      if (!key) return;
      if (key && key.ctrl && key.name === 'c') return app.endProcess(); 

      var command = getCommand(app.commands, key);
      app.doCommand(command);
      
      //app.renderScreen();
    }
  };
})();
