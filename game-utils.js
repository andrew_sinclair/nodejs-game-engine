(function() {
  var YAML = require('yamljs');
  
  function loadYaml(filename) {
    return nativeObject = YAML.load(filename);
  }

  function getRandomStream(values) {
    var streamGetter = function() {
      var index = Math.floor(Math.random() * values.length);

      return values[index];
    };

    return {get: streamGetter};
  }
  
  module.exports = {
    getRandomStream: getRandomStream,
    loadYaml: loadYaml
  };
})();