(function() {

  var 
    gamestate = {
      scene: []
      config: {},
      player: {} //TODO: make this guy a full blown object with methods
    }
    , inputInterface
    , Commands
    , exitHandler;

  function init(opts) {
    var commandLoader = opts.commandLoader
      , configLoader = opts.configLoader
      , cellProducer = opts.cellProducer
      , onInputHandler = opts.onInputHandler
      , onExitHandler = opts.inputProvider.onExitHandler
      , inputter = opts.inputProvider;

    initConfig(configLoader);
    initCommands(commandLoader);
    initPlayer();
    setInputInterface(inputter);
    initInput(onInputHandler, onExitHandler);
    initScreen(cellProducer);
  }

  function initConfig(configLoader) {
    gamestate.config = configLoader();
  }

  function initCommands(commandLoader) {
    Commands = commandLoader();
  }

  function initPlayer() {
    player.x = 0;
    player.y = 0;
    //TODO: player = new Player();
  }

  function initScreen(cellProducer) {
    screenSetForeach(cellProducer);
  }

  function setInputInterface(inputProvider) {
    inputInterface = inputProvider;
  }

  function initInput(onInput, onExitHandler) {
    inputInterface.registerEventListener(onInput);
    exitHandler = onExitHandler;
  }

  function wrapBorders(sceneStr) {
    function writeChar(character, num) {
      var outputChars = '';
      for (var i = 0; i < num; i++) {
        outputChars += character;
      }

      return outputChars;
    }

    var borders = gamestate.config.border
      , horiz_row = writeChar(borders.top_bottom, gamestate.config.max_width)
      , top_row = borders.top_left + horiz_row + borders.top_right
      , bottom_row = borders.bottom_left + horiz_row + borders.bottom_right
      , scene_rows = sceneStr.split(gamestate.config.new_line);
       

    scene_rows.pop();

    var mid_rows = scene_rows.map(function (row) {
      return borders.left_right + row + borders.left_right;
    });

    var outputRows = mid_rows;
    outputRows.unshift(top_row);
    outputRows.push(bottom_row);

    return outputRows.join(gamestate.config.new_line);
  }

  function renderScreen() {
    var cellIndex = 0
      , sceneOutput = '';

    screenGetForeach(function(cell) {
      sceneOutput += (cell + (++cellIndex % gamestate.config.max_width ? '' : gamestate.config.new_line));
    });

    if (gamestate.config.hasBorders) {
      sceneOutput = wrapBorders(sceneOutput);
    }

    //TODO: remove dependency on console
    console.log(sceneOutput);
  }

  function screenSetForeach(cellProducer) {
    var newRow;
    gamestate.scene = [];

    for (var i = 0; i < gamestate.config.max_height; i++) {
      newRow = [];
      gamestate.scene.push(newRow);

      for (var j = 0; j < gamestate.config.max_width; j++) {
        newRow.push((typeof cellProducer === 'function') ? cellProducer() : cellProducer);
      }
    }
  }

  function screenGetForeach(cellConsumer, callback) {
    if (typeof cellConsumer !== 'function') return console.log("Error! cellConsumer must be a function.");

    var nextRow;
    for (var i = 0; i < gamestate.config.max_height; i++) {
      nextRow = gamestate.scene[i];

      for (var j = 0; j < gamestate.config.max_width; j++) {
        cellConsumer(nextRow[j]);
      }
    }

    if(typeof callback === 'function') {
      return callback(gamestate.scene);
    }
  }

  function endProcess() {
    exitHandler();
  }

  function beginProcess() {
    renderScreen();
  }

  function doCommand(command) {
    command(gamestate);
    renderScreen();
  }

  module.exports = {
    init: init,
    renderScreen: renderScreen,
    endProcess: endProcess,
    beginProcess: beginProcess,
    doCommand: doCommand,
    commands: Commands //todo, return a clone so it's "constant"
  };
})();
